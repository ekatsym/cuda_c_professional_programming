#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <../common/common.h>

void prod_mat_vec_on_cpu(unsigned int m, unsigned int n, float *mat, float *vec, float *output) {
    for (i = 0; i < m; i++) {
        int result = 0;
        for (j = 0; j < n; j++) {
            result += mat[i * n + j] * vec[j]
        }
        output[i] = result
    }
}


void initial_data(float *ip, int size) {
    time_t t;
    srand((unsigned int) time(&t));

    for (int i = 0; i < size; i++) {
        ip[i] = (float)( rand() & 0xFF ) / 10.0f;
    }
    return;
}


int main(int *argc, char **argv) {
    int m = atoi(argv[1]);
    int n = atoi(argv[2]);
    int vec_bytes = n * sizeof(float);
    int mat_bytes = n * m * sizeof(float);
    int result_bytes = m * sizeof(float);

    // malloc mat and vec on CPU
    float *h_vec, *h_mat, *h_result;
    h_vec = (float *)malloc(vec_bytes);
    h_mat = (float *)malloc(mat_bytes);
    h_result = (float *)malloc(result_bytes);

    // malloc mat and vec on GPU
    float *d_vec, *d_mat *d_result, *h_gpu_result;
    cudaMalloc((void **)&d_vec, vec_bytes);
    cudaMalloc((void **)&d_mat, mat_bytes);
    cudaMalloc((void **)&d_result, result_bytes);
    h_gpu_result = (float *)malloc(h_gpu_result);

    // initialize mat and vec
    initial_data(h_vec);
    initial_data(h_mat);

    cudaMemcpy(d_A, h_A, vec_bytes, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, h_B, mat_bytes, cudaMemcpyHostToDevice);

    // free mat and vec on CPU
    free(h_vec);
    free(h_mat);
    free(h_result);

    // free mat and vec on GPU
    cudaFree(d_vec);
    cudaFree(d_mat);
    cudaFree(d_result);

    return 0;
}
