#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <functional>

#ifndef _COMMON_H
#define _COMMON_H

#define CHECK(call) {                                                           \
    const cudaError_t error = call;                                             \
    if (error != cudaSuccess) {                                                \
        fprintf(stderr, "Error: %s:%d, ", __FILE__, __LINE__);                  \
        fprintf(stderr, "code: %d, reason: %s\n", error,                        \
                cudaGetErrorString(error));                                     \
    }                                                                           \
}                                                                               \

#define CHECK_CUBLAS(call) {                                                    \
    cublasStatus_t err;                                                         \
    if ((err = (call)) != CUBLAS_STATUS_SUCCESS) {                              \
        fprintf(stderr, "Got CUBLAS error %d at %s:%d\n", err, __FILE__,        \
                __LINE);                                                        \
        exit(1);                                                                \
    }                                                                           \
}                                                                               \

#define CHECK_CURAND(call) {                                                    \
    curandStatus_t err;                                                         \
    if ((err = (call)) != CURAND_STATUS_SUCCESS) {                              \
        fprintf(stderr, "Got CURAND error %d at %s:%d\n", err, __FILE__,        \
                __LINE__);                                                      \
        exit(1);                                                                \
    }                                                                           \
}

#define CHECK_CUFFT(call) {                                                     \
    cufftResult err;                                                            \
    if ( (err = (call)) != CUFFT_SUCCESS) {                                     \
        fprintf(stderr, "Got CUFFT error %d at %s:%d\n", err, __FILE__,         \
                __LINE__);                                                      \
        exit(1);                                                                \
    }                                                                           \
}

#define CHECK_CUSPARSE(call) {                                                  \
    cusparseStatus_t err;                                                       \
    if ((err = (call)) != CUSPARSE_STATUS_SUCCESS) {                            \
        fprintf(stderr, "Got error %d at %s:%d\n", err, __FILE__, __LINE__);    \
        cudaError_t cuda_err = cudaGetLastError();                              \
        if (cuda_err != cudaSuccess) {                                          \
            fprintf(stderr, "  CUDA error \"%s\" also detected\n",              \
                    cudaGetErrorString(cuda_err));                              \
        }                                                                       \
        exit(1);                                                                \
    }                                                                           \
}

inline double seconds() {
    struct timeval tp;
    struct timezone tzp;
    int i = gettimeofday(&tp, &tzp);
    return ((double)tp.tv_sec + (double)tp.tv_usec * 1.e-6);
}

class StopWatch {
    public:
        StopWatch() {
            _state = NONE;
        }

        void RecordStart() {
            if (_state == RUNNING) {
                printf("Currently running\n");
            } else {
                _state = RUNNING;
                timespec_get(&_t0, TIME_UTC);
            }
        }

        void RecordStop() {
            if (_state == RUNNING) {
                _state = STOPPED;
                timespec_get(&_t1, TIME_UTC);
            } else {
                printf("Currently not running\n");
            }
        }

        void DisplayTime(const char *prefix) {
            if (_state == STOPPED) {
                double sec = _t1.tv_sec - _t0.tv_sec;
                double nsec = _t1.tv_nsec - _t0.tv_nsec;
                double t = sec + nsec * 1e-9;
                printf("%32s : %.9lf s! \n", prefix, t);
            } else {
                printf("Doesn't measured yet\n");
            }
        }

    private:
        enum{
            NONE,
            RUNNING,
            STOPPED,
        } _state;
        struct timespec _t0;
        struct timespec _t1;
};

void timeit (const char *name, std::function<void(void)> f) {
    StopWatch sw;
    sw.RecordStart();
    sw.RecordStop();
    sw.DisplayTime(name);
}

#endif // _COMMON_H
