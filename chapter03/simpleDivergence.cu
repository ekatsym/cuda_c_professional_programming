#include <stdio.h>
#include <cuda_runtime.h>
#include <sys/time.h>

#define CHECK(call) {                                   \
    const cudaError_t error = call;                     \
    if (error != cudaSuccess) {                         \
        printf("Error: %s:%d, ", __FILE__, __LINE__);   \
        printf("code:%d, reason: %s\n", error,          \
               cudaGetErrorString(error));              \
        exit(1);                                        \
    }                                                   \
}

double seconds() {
    struct timeval tp;
    struct timezone tzp;
    int i = gettimeofday(&tp, &tzp);
    return ((double)tp.tv_sec + (double)tp.tv_usec * 1.e-6);
}

__global__ void mathKernel1(float *c) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    float ia, ib;
    ia = ib = 0.0f;

    if (tid % 2 == 0) {
        ia = 100.0f;
    } else {
        ib = 200.0f;
    }
    c[tid] = ia + ib;
}

__global__ void mathKernel2(float *c) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    float ia, ib;
    ia = ib = 0.0f;

    if ((tid / warpSize) % 2 == 0) {
        ia = 100.0f;
    } else {
        ib = 200.0f;
    }
    c[tid] = ia + ib;
}

__global__ void mathKernel3(float *c) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    float ia, ib;
    ia = ib = 0.0f;

    bool ipred = (tid % 2 == 0);
    if (ipred) {
        ia = 100.0f;
    }
    if (!ipred) {
        ib = 200.0f;
    }
    c[tid] = ia + ib;
}

int main(int argc, char **argv) {
    // set up device
    int dev = 0;
    cudaDeviceProp deviceProp;
    CHECK(cudaGetDeviceProperties(&deviceProp, dev));
    printf("%s using Device %d: %s\n", argv[0], dev, deviceProp.name);

    // configure data size
    int size = 64;
    int blocksize = 64;
    if (argc > 1) {
        blocksize = atoi(argv[1]);
    }
    if (argc > 2) {
        size = atoi(argv[2]);
    }
    printf("Data size %d ", size);

    // set up execution configure
    dim3 block(blocksize);
    dim3 grid((size + block.x - 1) / block.x);
    printf("Execution Configure (block %d grid %d)\n", block.x, grid.x);

    // malloc GPU memory
    float *d_C;
    size_t nBytes = size * sizeof(float);
    CHECK(cudaMalloc((void**)&d_C, nBytes));

    // run mathKernel2 to remove overhead
    double iStart, iElaps;
    CHECK(cudaDeviceSynchronize());
    mathKernel2<<<grid, block>>>(d_C);
    CHECK(cudaDeviceSynchronize());
    CHECK(cudaGetLastError());

    // run mathKernel1
    CHECK(cudaDeviceSynchronize());
    iStart = seconds();
    mathKernel1<<<grid, block>>>(d_C);
    CHECK(cudaDeviceSynchronize());
    iElaps = seconds() - iStart;
    printf("mathKernel1<<<%4d, %4d>>> elapsed %lf sec \n",
           grid.x, block.x, iElaps);
    CHECK(cudaGetLastError());

    // run mathKernel2
    CHECK(cudaDeviceSynchronize());
    iStart = seconds();
    mathKernel2<<<grid, block>>>(d_C);
    CHECK(cudaDeviceSynchronize());
    iElaps = seconds() - iStart;
    printf("mathKernel2<<<%4d, %4d>>> elapsed %lf sec \n",
           grid.x, block.x, iElaps);
    CHECK(cudaGetLastError());

    // run mathKernel3
    iStart = seconds();
    mathKernel3<<<grid, block>>>(d_C);
    CHECK(cudaDeviceSynchronize());
    iElaps = seconds() - iStart;
    printf("mathKernel3<<<%4d, %4d>>> elapsed %lf sec \n",
           grid.x, block.x, iElaps);
    CHECK(cudaGetLastError());

    // free memory and reset device
    CHECK(cudaFree(d_C));
    CHECK(cudaDeviceReset());
    return EXIT_SUCCESS;
}
